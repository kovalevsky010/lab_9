import { combineReducers } from 'redux';
import {
  reducer as postsReducer,
  actions as postsActions,
  selectors as postsSelectors,
} from './posts';
import {
  reducer as commentsReducer,
  actions as commentsActions,
  selectors as commentsSelectors,
} from './comments';
import {
  reducer as usersReducer,
  actions as usersActions,
  selectors as usersSelectors,
} from './users';

/**
 * Берем все редюсеры из слайсев и объединяем в один
 */
export const rootReducer = combineReducers({
  posts: postsReducer,
  comments: commentsReducer,
  users: usersReducer,
});

/**
 * Переэкспортируем все экшены со всех слайсев, чтобы было удобнее
 * Так же с селекторами
 */
export const actions = { ...postsActions, ...commentsActions, ...usersActions };
export const selectors = {
  ...postsSelectors,
  ...commentsSelectors,
  ...usersSelectors,
};
