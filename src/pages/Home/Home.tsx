import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppRoutes } from '../../common/constants';
import { actions, selectors } from '../../store';
import './index.css';

export function Home() {
  //Получаем диспатчер, чтобы вызывать экшены
  const dispatch = useDispatch();

  //Через useSelector получаем 1) грузятся ли посты; 2) загруженные посты
  const postsLoadingStatus = useSelector(selectors.selectAllPostsLoadingStatus)
  const posts = useSelector(selectors.selectAllPosts);

  useEffect(() => {
    //При первом рендере запускаем загрузку постов
    dispatch(actions.fetchPosts(undefined));
  }, []);

  const renderPosts = () => {
    if (postsLoadingStatus === 'pending') {
      return <h1>Loading...</h1>;
    }

    return posts.map((post) => (
      <div className="post-container" key={post.id}>
        <a href={AppRoutes.POST(post.id)} className="post-title">
          {post.title}
        </a>
        <small>{post.body}</small>
      </div>
    ));
  };

  return (
    <div className="App">
      <main className="posts">{renderPosts()}</main>
    </div>
  );
}
