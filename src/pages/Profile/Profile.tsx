import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { actions, selectors } from '../../store';

/**
 * Пример как это все можно сделать 😉
 */

export function Profile() {
  const { id } = useParams();
  const userId = parseInt(id as string);

  const dispatch = useDispatch();
  const user = useSelector(selectors.selectUserById(userId));
  const loadingStatus = useSelector(selectors.selectUserLoadingStatus(userId));

  useEffect(() => {
    dispatch(actions.fetchUserById(userId));
  }, []);

  if (loadingStatus === 'pending') {
    return <div>Loading...</div>;
  }

  if (!user) {
    return <h1>User not found...</h1>;
  }

  return (
    <div>
      <h1>{user.name}</h1>
      <p>Phone: {user.phone}</p>
      <p>Email: {user.email}</p>
    </div>
  );
}
