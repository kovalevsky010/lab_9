import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { AppRoutes } from '../../common/constants';
import { actions, selectors } from '../../store';
import { CommentsList } from './components';
import './index.css';

export function Post() {
  /**
   * Вытаскиваем из URL-а ID поста
   */
  const { id } = useParams();
  /**
   * Получаем строку, поэтому нужно привести к числовому типу
   */
  const postId = parseInt(id as string);

  const dispatch = useDispatch();

  /**
   * Через useSelector получаем нужные данные
   * 1. пост по ID
   * 2. статус загрузки поста по ID
   * 3. комментарии к посту по ID
   * 4. автора поста по ID
   */
  const post = useSelector(selectors.selectSinglePost(postId));
  const postLoadingStatus = useSelector(
    selectors.selectSinglePostLoadingStatus(postId)
  );
  const postComments = useSelector(selectors.selectPostComments(postId));
  const author = useSelector(selectors.selectUserById(post?.userId!));

  useEffect(() => {
    if (postId) {
      /**
       * Диспатчим санку на загрузку поста по ID, по аналогии с диспатчем ниже
       */
      dispatch(actions.fetchSinglePost(postId));
      dispatch(actions.fetchCommentsByPostId(postId));
    }
  }, []);

  useEffect(() => {
    if (post) {
      /**
       * Диспатчим санку на подгрузку автора, когда пост уже загрузился
       */
      dispatch(actions.fetchUserById(post.userId));
    }
  }, [post]);

  if (postLoadingStatus === 'pending') {
    return <div>Loading...</div>;
  }

  if (!post) {
    return <h1>Post not found...</h1>;
  }

  return (
    <div>
      <h1>Post№ {post.id}</h1>
      <a href={AppRoutes.PROFILE(post.userId)}>{author?.name}</a>
      <h2>{post.title}</h2>
      <p>{post.body}</p>
      <CommentsList comments={postComments} />
    </div>
  );
}
